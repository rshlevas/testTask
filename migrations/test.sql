-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Авг 13 2017 г., 15:17
-- Версия сервера: 5.7.14
-- Версия PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `test`
--

-- --------------------------------------------------------

--
-- Структура таблицы `acl_classes`
--

CREATE TABLE `acl_classes` (
  `id` int(10) UNSIGNED NOT NULL,
  `class_type` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `acl_classes`
--

INSERT INTO `acl_classes` (`id`, `class_type`) VALUES
(1, 'AppBundle\\Admin\\SystemMessageAdmin'),
(2, 'AppBundle\\Admin\\UserAdmin'),
(3, 'AppBundle\\Entity\\SystemMessage'),
(4, 'AppBundle\\Entity\\User');

-- --------------------------------------------------------

--
-- Структура таблицы `acl_entries`
--

CREATE TABLE `acl_entries` (
  `id` int(10) UNSIGNED NOT NULL,
  `class_id` int(10) UNSIGNED NOT NULL,
  `object_identity_id` int(10) UNSIGNED DEFAULT NULL,
  `security_identity_id` int(10) UNSIGNED NOT NULL,
  `field_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ace_order` smallint(5) UNSIGNED NOT NULL,
  `mask` int(11) NOT NULL,
  `granting` tinyint(1) NOT NULL,
  `granting_strategy` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `audit_success` tinyint(1) NOT NULL,
  `audit_failure` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `acl_entries`
--

INSERT INTO `acl_entries` (`id`, `class_id`, `object_identity_id`, `security_identity_id`, `field_name`, `ace_order`, `mask`, `granting`, `granting_strategy`, `audit_success`, `audit_failure`) VALUES
(1, 1, NULL, 1, NULL, 0, 64, 1, 'all', 0, 0),
(2, 1, NULL, 2, NULL, 1, 8224, 1, 'all', 0, 0),
(3, 1, NULL, 3, NULL, 2, 4098, 1, 'all', 0, 0),
(4, 1, NULL, 4, NULL, 3, 4096, 1, 'all', 0, 0),
(5, 2, NULL, 5, NULL, 0, 64, 1, 'all', 0, 0),
(6, 2, NULL, 6, NULL, 1, 8224, 1, 'all', 0, 0),
(7, 2, NULL, 7, NULL, 2, 4098, 1, 'all', 0, 0),
(8, 2, NULL, 8, NULL, 3, 4096, 1, 'all', 0, 0),
(9, 3, NULL, 1, NULL, 0, 64, 1, 'all', 0, 0),
(10, 3, NULL, 2, NULL, 1, 32, 1, 'all', 0, 0),
(11, 3, NULL, 3, NULL, 2, 4, 1, 'all', 0, 0),
(12, 3, NULL, 4, NULL, 3, 1, 1, 'all', 0, 0),
(13, 4, NULL, 5, NULL, 0, 64, 1, 'all', 0, 0),
(14, 4, NULL, 6, NULL, 1, 32, 1, 'all', 0, 0),
(15, 4, NULL, 7, NULL, 2, 4, 1, 'all', 0, 0),
(16, 4, NULL, 8, NULL, 3, 1, 1, 'all', 0, 0),
(17, 4, 12, 9, NULL, 0, 128, 1, 'all', 0, 0),
(18, 4, 13, 9, NULL, 0, 128, 1, 'all', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `acl_object_identities`
--

CREATE TABLE `acl_object_identities` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_object_identity_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED NOT NULL,
  `object_identifier` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `entries_inheriting` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `acl_object_identities`
--

INSERT INTO `acl_object_identities` (`id`, `parent_object_identity_id`, `class_id`, `object_identifier`, `entries_inheriting`) VALUES
(1, NULL, 1, 'admin.systemMessage', 1),
(2, NULL, 2, 'admin.user', 1),
(3, NULL, 3, '5', 1),
(4, NULL, 3, '3', 1),
(5, NULL, 3, '4', 1),
(6, NULL, 3, '6', 1),
(7, NULL, 4, '1', 1),
(8, NULL, 4, '2', 1),
(9, NULL, 4, '4', 1),
(10, NULL, 4, '6', 1),
(11, NULL, 4, '7', 1),
(12, NULL, 4, '9', 1),
(13, NULL, 4, '10', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `acl_object_identity_ancestors`
--

CREATE TABLE `acl_object_identity_ancestors` (
  `object_identity_id` int(10) UNSIGNED NOT NULL,
  `ancestor_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `acl_object_identity_ancestors`
--

INSERT INTO `acl_object_identity_ancestors` (`object_identity_id`, `ancestor_id`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7),
(8, 8),
(9, 9),
(10, 10),
(11, 11),
(12, 12),
(13, 13);

-- --------------------------------------------------------

--
-- Структура таблицы `acl_security_identities`
--

CREATE TABLE `acl_security_identities` (
  `id` int(10) UNSIGNED NOT NULL,
  `identifier` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `username` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `acl_security_identities`
--

INSERT INTO `acl_security_identities` (`id`, `identifier`, `username`) VALUES
(9, 'AppBundle\\Entity\\User-adminuser', 1),
(1, 'ROLE_ADMIN_SYSTEMMESSAGE_ADMIN', 0),
(2, 'ROLE_ADMIN_SYSTEMMESSAGE_EDITOR', 0),
(4, 'ROLE_ADMIN_SYSTEMMESSAGE_GUEST', 0),
(3, 'ROLE_ADMIN_SYSTEMMESSAGE_STAFF', 0),
(5, 'ROLE_ADMIN_USER_ADMIN', 0),
(6, 'ROLE_ADMIN_USER_EDITOR', 0),
(8, 'ROLE_ADMIN_USER_GUEST', 0),
(7, 'ROLE_ADMIN_USER_STAFF', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `fos_user`
--

CREATE TABLE `fos_user` (
  `id` int(11) NOT NULL,
  `username` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `surname` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `sex` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `country` varchar(100) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `fos_user`
--

INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`, `name`, `surname`, `birthday`, `sex`, `country`) VALUES
(1, '3674558217', '3674558217', 'a@aa.aa', 'a@aa.aa', 1, NULL, '$2y$13$zFqcuF478GbafecTPpAMr.HldZ2ZoSVEVu82khldhz6ha.D4/bNCi', '2017-08-13 14:36:48', NULL, NULL, 'a:1:{i:0;s:12:"ROLE_SUPPORT";}', 'Хан', 'Соло', '1920-11-18', 'Мужской', 'Корусант'),
(2, NULL, NULL, 'b@bb.bb', 'b@bb.bb', 1, NULL, '$2y$13$unknnaK3q84Cp1Ks5.bJ5O9FLyjISnlZptpPasQKySfdjEeQdmh6S', '2017-08-12 12:38:49', NULL, NULL, 'a:0:{}', NULL, NULL, NULL, NULL, NULL),
(4, 'adminuser', 'adminuser', 'admin@admin.com', 'admin@admin.com', 1, NULL, '$2y$13$xcFfZG8yoy5PGn/QIkSjxOTSMlRO6A6FgSh5F39Yum5h37uwrqPWe', '2017-08-13 14:35:08', NULL, NULL, 'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}', NULL, NULL, NULL, NULL, NULL),
(6, '123123122', '123123122', '23@add.com', '23@add.com', 1, NULL, '$2y$13$yc0ZXRN.kzlmyKgMixtoR.s0yyA1Lgx/JDxQznkcb2myIlwfUUJ42', '2017-08-07 12:40:52', NULL, NULL, 'a:0:{}', 'asdsddddsdddd', 'asdd', '1920-05-19', 'Мужской', 'asdddwdasd'),
(7, NULL, NULL, 'asdd@addasd.asdd', 'asdd@addasd.asdd', 1, NULL, '$2y$13$hVT2rq3DYV9JTaMc6DnJROh.NxzW2FBDxyqW2MjbeoSGmHGMxqkjO', NULL, NULL, NULL, 'a:1:{i:0;s:12:"ROLE_SUPPORT";}', NULL, NULL, NULL, NULL, NULL),
(8, '111111', '111111', 'bb@aa.aa', 'bb@aa.aa', 1, NULL, '$2y$13$CQ8qGRqno5VvObw4z.zqT.jAyUNEMJ.mSoxiaupWl/eRnZA/9x09e', '2017-08-13 11:35:09', NULL, NULL, 'a:0:{}', 'Василий', 'Васильев', NULL, NULL, NULL),
(10, '12151515', '12151515', NULL, NULL, 1, NULL, '$2y$13$AFiOMV6eT.XO4.Q4ufc4SOMJ4R4JeeiZ.OXt0AxVF9ApEEdQXE9DG', NULL, NULL, NULL, 'a:0:{}', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `system_message`
--

CREATE TABLE `system_message` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `send_type` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `content` text NOT NULL,
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `system_message`
--

INSERT INTO `system_message` (`id`, `user_id`, `send_type`, `type`, `content`, `date`) VALUES
(5, 6, 1, 0, 'Выслан код активации', '2017-08-07'),
(3, 1, 0, 0, 'Выслан код активации', '2017-08-06'),
(4, 2, 0, 0, 'Выслан код активации', '2017-08-06'),
(6, 7, 0, 0, 'Выслан код активации:222634', '2017-08-07');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `acl_classes`
--
ALTER TABLE `acl_classes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_69DD750638A36066` (`class_type`);

--
-- Индексы таблицы `acl_entries`
--
ALTER TABLE `acl_entries`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_46C8B806EA000B103D9AB4A64DEF17BCE4289BF4` (`class_id`,`object_identity_id`,`field_name`,`ace_order`),
  ADD KEY `IDX_46C8B806EA000B103D9AB4A6DF9183C9` (`class_id`,`object_identity_id`,`security_identity_id`),
  ADD KEY `IDX_46C8B806EA000B10` (`class_id`),
  ADD KEY `IDX_46C8B8063D9AB4A6` (`object_identity_id`),
  ADD KEY `IDX_46C8B806DF9183C9` (`security_identity_id`);

--
-- Индексы таблицы `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_9407E5494B12AD6EA000B10` (`object_identifier`,`class_id`),
  ADD KEY `IDX_9407E54977FA751A` (`parent_object_identity_id`);

--
-- Индексы таблицы `acl_object_identity_ancestors`
--
ALTER TABLE `acl_object_identity_ancestors`
  ADD PRIMARY KEY (`object_identity_id`,`ancestor_id`),
  ADD KEY `IDX_825DE2993D9AB4A6` (`object_identity_id`),
  ADD KEY `IDX_825DE299C671CEA1` (`ancestor_id`);

--
-- Индексы таблицы `acl_security_identities`
--
ALTER TABLE `acl_security_identities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8835EE78772E836AF85E0677` (`identifier`,`username`);

--
-- Индексы таблицы `fos_user`
--
ALTER TABLE `fos_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`),
  ADD UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479C05FB297` (`confirmation_token`);

--
-- Индексы таблицы `system_message`
--
ALTER TABLE `system_message`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `acl_classes`
--
ALTER TABLE `acl_classes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `acl_entries`
--
ALTER TABLE `acl_entries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT для таблицы `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT для таблицы `acl_security_identities`
--
ALTER TABLE `acl_security_identities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `fos_user`
--
ALTER TABLE `fos_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `system_message`
--
ALTER TABLE `system_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `acl_entries`
--
ALTER TABLE `acl_entries`
  ADD CONSTRAINT `FK_46C8B8063D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_46C8B806DF9183C9` FOREIGN KEY (`security_identity_id`) REFERENCES `acl_security_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_46C8B806EA000B10` FOREIGN KEY (`class_id`) REFERENCES `acl_classes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
  ADD CONSTRAINT `FK_9407E54977FA751A` FOREIGN KEY (`parent_object_identity_id`) REFERENCES `acl_object_identities` (`id`);

--
-- Ограничения внешнего ключа таблицы `acl_object_identity_ancestors`
--
ALTER TABLE `acl_object_identity_ancestors`
  ADD CONSTRAINT `FK_825DE2993D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_825DE299C671CEA1` FOREIGN KEY (`ancestor_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
