## Description:

   Results of the test task solution 

## Installation:

   php composer.phar install -o
   
   *Note: mailer_user, bitcoin_user, bitcoin_password, bitcoin_port, epochta_user and epochta_password
          parameters should be defined or mistake will be displayed during project running


## Structure:

   migrations/*    - dump for DB
 
   app/*           - files with configurations and templates of the project

   src/*           - the main project code
   
   web/*           - fonts, css, js and images of the project

   features/*      - behat tests


## Configuration:

   If you used dump tables, that are represented in the migration folder: 
   
   SuperAdmin       - username: adminuser
                      password: aspirine

   Support          - username: a@aa.aa
                      password: aa

   User             - 1) username: b@bb.bb
                         password: bb
                      2) username: 111111
                         password: 11111

## Console:
   
    Console commands for console section of this task

    bitcoin:new-address             - Creating new address 

    bitcoin:balance                 - Show wallet balance 

    bitcoin:send <address> <amount> - Send some <amount> of bitcoins to some <address> 