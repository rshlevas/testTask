<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

/**
 * Class that could be used for transaction of some bitcoin amount
 * to some address by console command
 *
 * @category CommandClass
 * @package   
 * @author    
 * @license  
 * @link     
 */
class BitcoinSendCommand extends ContainerAwareCommand
{
    /**
     * Function that configure console command
     *  
     * @return void
     */
    protected function configure()
    {
        $this->setName('bitcoin:send')
            ->setDescription('Send bitcoins to some address')
            ->setHelp('This command send bitcoins to address')
            ->addArgument('address', InputArgument::REQUIRED, 'Address of wallet')
            ->addArgument('amount', InputArgument::REQUIRED, 'Summ of bitcoins');
    }
    
    /**
     * Function that execute console command
     * 
     * @param  InputInterface  $input  <p>InputInterface object</p>
     * @param  OutputInterface $output <p>OutputInterface object</p>
     * @return integer                 <p>0 if transaction pathed succssesful
     *                                    or code of error if it peresented</p>
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $bitcoin = $this->getContainer()->get('bitcoin_service');
        $result = $bitcoin->sendToAddress(
            $input->getArgument('address'),
            $input->getArgument('amount')    
        );
        if (!$result['result']) {
            $output->writeln($result['error']['message']);
            return $result['error']['code'];
        }
        $output->writeln('Your txid is ' . $result['result']);
        return 0;
    }
}
