<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

/**
 * Class that could be used create new address for bitcoin transaction
 * by console command
 *
 * @category CommandClass
 * @package   
 * @author    
 * @license  
 * @link     
 */
class BitcoinNewAdressCommand extends ContainerAwareCommand
{
    /**
     * Function that configure console command
     * 
     * @return void
     */
    protected function configure()
    {
        $this->setName('bitcoin:new-address')
            ->setDescription('Create new address')
            ->setHelp('This command display new address for transaction');
    }

    /**
     * Function that execute console command
     * 
     * @param  InputInterface  $input  <p>InputInterface object</p>
     * @param  OutputInterface $output <p>OutputInterface object</p>
     * @return integer                 <p>0 if address was created and showed or 
     *                                    code of error if it peresented</p>
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $bitcoin = $this->getContainer()->get('bitcoin_service');
        $address = $bitcoin->getNewAddress();
        if (!$address['result']) {
            $output->writeln($address['error']['message']);
            return $address['error']['code'];
        }
        $output->writeln('The adress is '.$address['result']);
        return 0;
    }
}
