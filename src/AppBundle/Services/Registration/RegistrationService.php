<?php

namespace AppBundle\Services\Registration;

use AppBundle\Entity\SystemMessage;
use Symfony\Component\HttpFoundation\Session\Session;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class that used for creating of new users
 * and messages, that important for success 
 * completing of registration
 *
 * @category ServiceClass
 * @package   
 * @author    
 * @license  
 * @link     
 */
class RegistrationService
{
    private $_session;

    private $_em;

    private $_data;

    private $_userManager;
    
    /**
     * Inial function
     * 
     * @param Session                $session 
     * @param EntityManagerInterface $em      
     * @param array                  $data        Array with user information</p>
     * @param UserManagerInterface   $userManager UserManagerInterface
     */
    public function __construct(
        Session $session, 
        EntityManagerInterface $em, 
        $data, 
        $userManager
    ) {
        $this->_session = $session;
        $this->_em = $em;
        $this->_data = $data;
        $this->_userManager = $userManager;
    }
    
    /**
     * Function that insert new user into the DB
     * 
     * @return boolean <p>True if user was created or false if wasn't</p>
     */
    public function createUser()
    {   
        $user = $this->_userManager->createUser();
        if ($this->_session->get('phone')) {
            $user->setUsername($this->_session->get('phone'));
            $user->setUsernameCanonical($this->_session->get('phone'));
        }
        if ($this->_session->get('email')) {
            $user->setEmail($this->_session->get('email'));
            $user->setEmailCanonical($this->_session->get('email'));
        }
        $user->setEnabled(1);
        $user->setPlainPassword($this->_data['plainPassword']);
        if (!$this->_userManager->updateUser($user)) {
            $this->createRegistrationMessage($user);
            $this->_session->clear();
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Function that insert activation message to the DB
     * 
     * @param AppBundle/Entity/User $user   
     * @return boolean 
     */
    protected function createRegistrationMessage($user) 
    {
        $message = new SystemMessage();
        $message->setContent('Выслан код активации:'.$this->_session->get('code'));
        $message->setType(0);
        if ($user->getUsername()) {
            $message->setSendType(1);
        }
        if ($user->getEmail()) {
            $message->setSendType(0);
        }
        $message->setDate(new \DateTime);
        $message->setUserId($user->getId());
        $message->setUser($user);
        $this->_em->persist($message);
        $this->_em->flush();
        return true;
    }
}
    

