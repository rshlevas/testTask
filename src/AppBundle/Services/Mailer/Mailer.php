<?php

namespace AppBundle\Services\Mailer;

/**
 * Class that used to send Email
 *
 * @category ServiceClass
 * @package   
 * @author    
 * @license  
 * @link     
 */
class Mailer
{
    /*
     * Email to send
     */
    protected $sendTo;

    /*
     * Email body
     */
    protected $body;

    /**
     * Inial function
     *     
     * @param string $sendTo Email where message will be sent
     * @param array  $body   Message body
     */
    public function __construct($sendTo, $body) 
    {
        $this->sendTo = $sendTo;
        $this->body = $body;
    }
    
    /**
     * Function that send message
     *     
     * @param  \Swift_Mailer $mailer \Swift_Mailer  
     * @return void
     */
    public function send($mailer)
    {   
        $message = $this->createMessage();
        $mailer->send($message);
        return 0;
    }
    
    /**
     * Function that create message
     *     
     * @return \Swift_Message
     */
    protected function createMessage()
    {
        return (new \Swift_Message('Hello Email'))
        ->setFrom('send@example.com')
        ->setTo($this->sendTo)
        ->setBody($this->body, 'text/html');
    }
}
    

