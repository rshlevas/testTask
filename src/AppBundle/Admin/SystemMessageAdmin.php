<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

/**
 * Class that is integrated with SonataAdminBudnle
 * and used for administration of Entity/SystemMessage Class 
 *
 * @category AdminClass
 * @package   
 * @author    
 * @license  
 * @link     
 */
class SystemMessageAdmin extends AbstractAdmin
{
    /**
     * Function that builds form for create and edit actions 
     * of admin panel
     * 
     * @param  FormMapper $formMapper FormMapper object
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('content', 'textarea', ['label' => 'Содержание'])
            ->add(
                'sendType', 
                'sonata_type_choice_field_mask', 
                [
                    'choices' => [
                        'На почту' => '0',
                        'На телефон' => '1',
                    ],
                    'map' => [
                        '0' => ['0', 'parameters'],
                        '1' => ['1'],
                    ],
                    'placeholder' => 'Укажите параметр',
                    'required' => false,
                    'label' => 'Тип отправки',
                ]
            )->add(
                'type', 
                'sonata_type_choice_field_mask', 
                [
                    'choices' => [
                        'Активация' => '0',
                        'Сообщение от администрации' => '1',
                        'Предупреждение' => '2'
                    ],
                    'map' => [
                        '0' => ['0', 'parameters'],
                        '1' => ['1'],
                        '2' => ['2'],
                    ],
                    'placeholder' => 'Укажите параметр',
                    'required' => false,
                    'label' => 'Вид сообщения',
                ]
            )->add(
                'user', 
                'entity', 
                [
                    'class' => 'AppBundle\Entity\User',
                    'label' => 'Пользователь',
                ]
            )->add('date', 'date');
    }
    
    /**
     * Function that configure filters
     * 
     * @param  DatagridMapper $datagridMapper DatagridMapper object
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('content')->add('date');
    }

    /**
     * Function that configure the table with list of object 
     * at admin panel
     * 
     * @param  ListMapper $listMapper ListMapper object
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('content', null, ['label' => 'Содержание'])
            ->add('user', null, ['label' => 'Пользователь'])
            ->add('date', null, ['label' => 'Дата отправки'])
            ->add(
                'sendType', 
                'choice', 
                [
                    'choices' => [
                        '0' => 'На почту',
                        '1' => 'На телефон',
                    ],
                    'label' => 'Тип отправки'
                ]
            )->add(
                'type', 
                'choice', 
                [
                    'choices' => [
                        '0' => 'Активация',
                        '1' => 'Сообщение от администрации',
                        '2' => 'Предупреждение',
                    ],
                    'label' => 'Вид сообщения'
                ]
            )->add(
                '_action', 
                null, 
                [
                    'actions' => [
                        'show' => [],
                        'edit' => [],
                        'delete' => [],
                    ],
                    'label' => 'Действия'
                ]
            );
    }

    /**
     * Function that configure the show action
     * of admin panel
     * 
     * @param  ShowMapper $showMapper ShowMapper object
     * @return void
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('content')
            ->add('date')
            ->add('user')
            ->add(
                'sendType', 
                'choice', 
                [
                    'choices' => [
                        '0' => 'На почту',
                        '1' => 'На телефон',
                    ]
                ]
            )->add(
                'type', 
                'choice', 
                [
                    'choices' => [
                        '0' => 'Активация',
                        '1' => 'Сообщение от администрации',
                        '2' => 'Предупреждение',
                    ]
                ]
            );
    }
}