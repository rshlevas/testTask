<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Storage of SystemMessage object.
 * 
 * @ORM\Entity
 * @ORM\Table(name="system_message")
 */
class SystemMessage
{
    /**
     * Id of the message
     * 
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * Id of the User related to this message
     * 
     * @ORM\Column(type="integer")
     */
    private $user_id;
    
    /**
     * Type of the message 
     * 0 - Activation, 
     * 1 - Message from administration
     * 2 - Message about blockage of user
     * 
     * @ORM\Column(type="integer")
     */
    private $type;
    
    /**
     * Described how message was sent
     * 0 - by email
     * 1 - by sms
     * 
     * @ORM\Column(type="integer")
     */
    private $send_type;
    
    /**
     * Content of the message
     * 
     * @ORM\Column(type="text")
     */
    private $content;
    
    /**
     * Date when message was sent
     * 
     * @ORM\Column(type="date")
     */
    private $date;
    
    /**
     * Entity/User object related to this message object
     * 
     * @ORM\ManyToOne(targetEntity="User", inversedBy="messages")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;
    
    /**
     * Returns id of the object
     * 
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set the if of the object
     * 
     * @param  integer $id <p>Id of the object</p>
     * @return integer
     */
    public function setId($id)
    {
        return $this->id = $id;
    }
    
    /**
     * Returns id of the user that related to this object
     * 
     * @return integer
     */
    public function getUserId()
    {
        return $this->user_id;
    }
    
    /**
     * Sets id of the user that related to this object
     * 
     * @param  integer $user_id <p>Id of the user that related to this object</p>
     * @return integer
     */
    public function setUserId($user_id)
    {
        return $this->user_id = $user_id;
    }
    
    /**
     * Returns type of message
     * 
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * Sets type of message
     * 
     * @param  integer $type <p>Type of the message</p>
     * @return integer
     */    
    public function setType($type)
    {
        return $this->type = $type;
    }
    
    /**
     * Returns id of how message was sent
     * 
     * @return integer
     */
    public function getSendType()
    {
        return $this->send_type;
    }
    
    /**
     * Sets id of how message was sent
     * 
     * @param  integer $send_type <p>Type of the message sending</p>
     * @return integer
     */ 
    public function setSendType($send_type)
    {
        return $this->send_type = $send_type;
    }
    
    /**
     * Returns content of message
     * 
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Sets content of message
     * 
     * @param  string $content <p>Content of the message</p>
     * @return string
     */     
    public function setContent($content)
    {
        return $this->content = $content;
    }
    
    /**
     * Returns date of message sending
     * 
     * @return \DateTime object
     */
    public function getDate()
    {
        return $this->date;
    }
    
    /**
     * Sets date of message sending
     * 
     * @param  \DateTime $dueDate <p>Date when message was sent</p>
     * @return \DateTime 
     */
    public function setDate(\DateTime $dueDate = null)
    {
        return $this->date = $dueDate;
    }
    
    /**
     * Returns user related to this message
     * 
     * @return \Entity\User object
     */
    public function getUser()
    {
        return $this->user;
    }
    
    /**
     * Returns user related to this message
     * 
     * @param  \Entity\User $user <p>User related to this message</p>
     * @return \Entity\User 
     */
    public function setUser(User $user) 
    {
        return $this->user = $user;
    }
    
    /**
     * @return string
     */
    public function __toString() 
    {
        return (string) $this->content;
    }
}