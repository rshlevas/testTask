<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Storage of SystemMessage object.
 * 
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * Id of the user
     * 
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * Name of the user
     * 
     * @ORM\Column(type="string", length=100)
     * @Assert\Length(max=100)
     */
    private $name;
    
    /**
     * Surname of the user
     * 
     * @ORM\Column(type="string", length=100)
     * @Assert\Length(max=100)
     */
    private $surname;    
    
    /**
     * Sex of the user
     * 
     * @ORM\Column(type="string", length=10)
     * @Assert\Length(max=10)
     */
    private $sex;
    
    /**
     * Day of birth of the user
     * 
     * @ORM\Column(type="date")
     */
    private $birthday;
    
    /**
     * Country of user
     * 
     * @ORM\Column(type="string", length=100)
     * @Assert\Length(max=100)
     */
    private $country;
    
    /**
     * Entity/SystemMessage objects related to this user
     * 
     * @ORM\OneToMany(targetEntity="SystemMessage", mappedBy="user")
     */
    private $messages;
    
    /**
     * Intial function
     * 
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->messages = new ArrayCollection();
    }
    
    /**
     * Returns name of the user
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Sets name of the user
     * 
     * @param  string $name <p>Name of the user</p>
     * @return string
     */
    public function setName($name)
    {        
        return $this->name = $name;
    }
    
    /**
     * Returns surname of the user
     * 
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }
    
    /**
     * Sets surname of the user
     * 
     * @param  string $surname <p>Surname of the user</p>
     * @return string
     */
    public function setSurname($surname)
    {        
        return $this->surname = $surname;
    }
    
    /**
     * Returns sex of the user
     * 
     * @return string
     */
    public function getSex()
    {
        return $this->sex;
    }
    
    /**
     * Sets sex of the user
     * 
     * @param  string $sex <p>Sex of the user</p>
     * @return string
     */
    public function setSex($sex)
    {        
        return $this->sex = $sex;
    }
    
    /**
     * Returns birthday of the user
     * 
     * @return string
     */
    public function getBirthday()
    {
        return $this->birthday;
    }
    
    /**
     * Sets birthday of the user
     * 
     * @param  string $birthday <p>Birthday of the user</p>
     * @return string
     */
    public function setBirthday($birthday)
    {        
        return $this->birthday = $birthday;
    }
    
    /**
     * Returns country of the user
     * 
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }
    
    /**
     * Sets country of the user
     * 
     * @param  string $country <p>Country of the user</p>
     * @return string
     */
    public function setCountry($country)
    {        
        return $this->country = $country;
    }
    
    /**
     * Returns messages related to this user
     * 
     * @return ArrayCollection
     */
    public function getMessages() 
    {
        return $this->messages;
    }
    
    /**
     * @return string
     */
    public function __toString()
    {
        if ($this->email) {
            return (string) $this->email;
        } elseif ($this->username) {
            return (string) $this->username;
        } else {
            return "NULL";
        }
    }
}