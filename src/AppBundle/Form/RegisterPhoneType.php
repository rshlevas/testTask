<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Regex;

/**
 * Class that could be used for building form 
 * with phone number input 
 *
 * @category AbstractTypeClass
 * @package   
 * @author    
 * @license  
 * @link     
 */
class RegisterPhoneType extends AbstractType
{
    /**
     * Function that builds form
     * 
     * @param FormBuilderInterface $builder 
     * @param array                $options Array with options (could be empty)
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'phone', TextType::class, [
                    'label' => 'Введите Ваш номер телефона',
                    'constraints' => [
                        new Length(
                            [
                                'min' => 12,
                                'max' => 12,
                                'exactMessage' => 
                                    'Введенный номер должен быть в формате 380505953139'
                            ]
                        ),
                        new Regex(
                            [
                                'pattern'   => '/^[0-9]*$/',
                                'match'     => true,
                                'message'   => 'Только цифры'
                            ]
                        )
                    ],
                    'invalid_message' => 
                        'Введенный номер должен быть в формате 380505953139'
                ]
            )->add('save', SubmitType::class, ['label' => 'Подтвердить']);
    }
}