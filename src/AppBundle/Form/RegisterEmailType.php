<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

/**
 * Class that could be used for building form 
 * with email input 
 *
 * @category AbstractTypeClass
 * @package   
 * @author    
 * @license  
 * @link     
 */

class RegisterEmailType extends AbstractType
{
    /**
     * Function that builds form
     * 
     * @param FormBuilderInterface $builder 
     * @param array                $options Array with options (could be empty)
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, ['label' => 'Введите Ваш email'])
            ->add('save', SubmitType::class, ['label' => 'Подтвердить']);
    }
}