<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\User;
use AppBundle\Entity\Wallet;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use AppBundle\Form\UserProfileInfoType;
use AppBundle\Form\RegisterEmailType;
use AppBundle\Form\UserProfilePhoneEditType;
use AppBundle\Services\Bitcoin\BitcoinService;

/**
 * Class related to Controller category and used 
 * for routes with /profile prefix
 *
 * @category Controller
 * @package   
 * @author    
 * @license  
 * @link     
 */
class ProfileController extends Controller
{
    /** 
     * @Route("/profile/show", name="fos_user_profile_show")
     */
    public function showAction()
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute('homepage');
        }
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SUPPORT')) {
            return $this->redirectToRoute('sonata_admin_redirect');
        }
        $user = $this->getUser();
        return $this->render(
            'profile/view.html.twig', 
            [
                'user' => $user
            ]
        );
    }
       
    /**
     * @Route("/profile/edit", name="profile_edit")
     */
    public function editAction(Request $request) 
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute('homepage');
        }
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SUPPORT')) {
            return $this->redirectToRoute('sonata_admin_redirect');
        }
        $user = $this->getUser();
        $form = $this->createForm(UserProfileInfoType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $userManager = $this->get('fos_user.user_manager');
            $userManager->updateUser($user);
            return $this->redirectToRoute('fos_user_profile_show');
        }
        return $this->render(
            'profile/edit.html.twig', 
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/profile/security", name="profile_security")
     */
    public function securityAction(Request $request) 
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute('homepage');
        }
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SUPPORT')) {
            return $this->redirectToRoute('sonata_admin_redirect');
        }
        $user = $this->getUser();
        $formEmail = $this->createForm(RegisterEmailType::class, $user);
        $formEmail->handleRequest($request);
        if ($formEmail->isSubmitted() && $formEmail->isValid()) {
            $user = $formEmail->getData();
            $userManager = $this->get('fos_user.user_manager');
            $userManager->updateUser($user);
            return $this->redirectToRoute('fos_user_profile_show');
        }
        $formPhone = $this->createForm(UserProfilePhoneEditType::class, $user);
        $formPhone->handleRequest($request);
        if ($formPhone->isSubmitted() && $formPhone->isValid()) {
            $user = $formPhone->getData();
            $userManager = $this->get('fos_user.user_manager');
            $userManager->updateUser($user);
            return $this->redirectToRoute('fos_user_profile_show');
        }
        return $this->render(
            'profile/security.html.twig', 
            [
                'formEmail' => $formEmail->createView(),
                'formPhone' => $formPhone->createView(),
            ]
        );
    }
}