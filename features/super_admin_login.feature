Feature: Login as SuperAdmin and see, where we'll be

Scenario: login with user SuperAdmin role
    Given I am on "/login"
    When I fill in "_username" with "adminuser"
    When I fill in "_password" with "aspirine"
    When I press "_submit"
    Then I should be on "/admin/dashboard"